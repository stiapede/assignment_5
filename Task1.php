

1
$x('//Disney/Subsidiaries/Subsidiary[@id="WaltDisneyPictures"]/Movie[Year>2010][ScreenTime>90]')

2.
Finner
$x('//Disney/Subsidiaries/Subsidiary[Movie[Name="Iron Man"]/Cast/Role[@name="Tony Stark"]]')

3.
$x('//Disney/Actors/Actor[contains(BirthPlace, "England, UK")]')

4.
$x('//Disney/Subsidiaries/Subsidiary/Movie[Cast[count(Role[@alias= "Iron Man" or @alias= "Spider-Man" or @alias="Captain America" or @alias="Hulk"])>2]]')

5.
$x('sum(//Disney/Subsidiaries/Subsidiary/Movie[contains(Name, "Star Wars")]/ScreenTime)')

6.
$x('//Disney/Actors/Actor[contains(BirthPlace, "USA") and not(contains(BirthPlace, "California"))]')

7.
$x('//Disney/Subsidiaries/Subsidiary/Movie[Cast/Role[contains(@name, "Skywalker")]]')

8.
$x('//Disney/Subsidiaries/Subsidiary/Movie[Name="The Lion King"]/Cast/Role[@name="Simba"][@actor]')
